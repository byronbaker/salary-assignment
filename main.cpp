#include <iostream>

using namespace std;

struct Employee {
    int empId;
    string fName;
    string lName;
    double payRate;
    int hours;
};

int main()
{
    int numEmploy;
    double grossPayTotal = 0;
    //It was probably faster to do the extra credit because it cut back signifigantly on testing.
    cout << "How many employees do you want to record?:";
    cin >> numEmploy;
    Employee added[numEmploy];
    for(int i = 0; i < numEmploy; i++) {
        cout << "Enter Employee " << i+1 << "'s ID:" ;
        cin >> added[i].empId;
        cout << "Enter Employee " << i+1 << "'s First Name:";
        cin >> added[i].fName;
        cout << "Enter Employee " << i+1 << "'s Last Name:";
        cin >> added[i].lName;
        cout << "Enter Employee " << i+1 << "'s Pay Rate:";
        cin >> added[i].payRate;
        cout << "Enter Employee " << i+1 << "'s Hours:";
        cin >> added[i].hours;
    }
    for(int i = 0; i < numEmploy; i++) {
        cout << "\nEmployee " << i+1 << "'s ID: ";
        cout << added[i].empId;
        cout << "\nEmployee " << i+1 << "'s Name: ";
        cout << added[i].fName << " " << added[i].lName;
        cout << "\nEmployee " << i+1 << "'s Gross Pay: ";
        double grossPay = added[i].payRate*added[i].hours;
        cout << grossPay;
        grossPayTotal += grossPay;
    }
    cout << "\nTotal Gross Pay: " << grossPayTotal;
}